**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).


# What does this program do?
Task One: Data Fields And Source Of Data
Planning ahead what data fields and attributes we’ll need to satisfy the requirements. Also, looking for websites where I can get data from.

Task Two: Scrapy Spiders
Creating scrapers for the website(s) that we’ve chosen in the previous task.

Task Three: Process Data
Cleaning, standardizing, normalizing, structuring and storing data into a database.

Task Four: Analyze Data
Creating reports that help you make decisions or help you understand data more.

Task Five: Conclusions
Draw conclusions based on analysis. Understand data.


Task Zero: Requirements Of Reports
As I said we’re gonna analyze college football data. I think though “college football data” is too big of a scope for us right now so let’s say we wanna analyze only touchdowns and game results. We consider games that have been played in the latest college football season (2017-18). Regarding college football games, it really depends on how good of a data source we can find. If we could find a website with roughly all the college football teams major ones(D1 colleges) that would be the best.

What kind of reports to produce?
What kind of reports we’re gonna create to know exactly what to scrape. 

We’re focusing on goals and results. It would be interesting to see some basic overall reports like:

Average amount of Touchdowns
Average amount of 1st half/2nd half touchdowns
Amount of home wins/away wins/ties
The biggest touchdown difference between two teams
The biggest comeback in the 2nd half
Distribution of average touchdowns along the season
We’re gonna scrape data only to be able to produce these reports.

Task One: Source Of Data And Data Fields
We want to find a website which has all the data fields we need. What are our data fields? We can figure them out having a close look at the report requirements we just wrote. Data fields are data points and they will be scraped by our future scraper. Putting the data fields together we’ll get a Scrapy item or a record in the database.

Going through the report requirements we will need at least these fields:

Home team touchdowns 1st half
Home team touchdowns 2nd half
Away team touchdowns 1st half
Away team touchdownshalf
Game date
Division
State
Scraping only these fields will be enough to generate some interesting reports about football games in the latest season.

Looking for a data source
The next step is to find the source of our raw data aka a website we can scrape. So let’s do a research!

I simply start off googling college footall stats.




